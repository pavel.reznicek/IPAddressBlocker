# IP Address Blocker
---

This is originally a tiny Python 3 script to collect unwanted IP addresses 
from [Aleksandr Morozovʼs “block-shodan-stretchoid-census” repository](https://gitlab.com/ohisee/block-shodan-stretchoid-census)
and block them via UFW.

Can be used to block IP addresses from simple lists 
written in text files. The lists consist of single IP addresses, one per line,
or net masks of the form `xxx.xxx.xxx.xxx[/xx]`. Whitespace is ignored.
The first hash sign in a line and anything following it is ignored too so this
way you can comment as much as you want.

Currently the list files have to obey the file name mask of `pf_table_*.txt`

An example IP address list file is included (currently it is named 
`pf_table_local.txt`).

## Requirements

You need Python 3 and UFW (The Uncomplicated Firewall) installed on your system 
(which should be a Linux machine or any that these programs run on).

## Running

```bash
cd <directory with the list files>
<path to this repositoryʼs clone>/ip_address_blocker.py
```
Note: The script should be executable. If it cannot be, prepend `python3`
before the path on the second line.

At the time being, UFW rules get added before the start of the ruleset,
in reversed order, in order to override your global permissive rules
that should go later in the list.

Happy blocking,

Cigydd
