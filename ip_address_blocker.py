#!/usr/bin/env python3

import glob
import os
#import re
import ipaddress

IPV4 = 4
IPV6 = 6

def get_filenames():
    names = sorted(glob.glob('./pf_table_*.txt'))
    return names

def get_ipas_from_file(filename):
    ipas = {
        IPV4: [],
        IPV6: []
    }
    ipa_file = open(filename, 'r')
    lines = ipa_file.readlines()
    for line in lines:
        # remove comments
        hashsign = '#'
        hashsignpos = line.find(hashsign)
        if hashsignpos > -1:
            comment = line[hashsignpos + 1:]
            line = line[:hashsignpos]
        else:
            comment = ''
        line = line.strip()
        comment = comment.strip()
        if line:
            #pattern = \
            #r"(\d{1,3})[.](\d{1,3})[.](\d{1,3})[.](\d{1,3})" \
            #r"(?:/(\d{1,2}))?"
            #match = re.fullmatch(
            #    pattern,
            #    line
            #)
            #if match:
            #    intipa = ()
            #    for g in range(1, 5): # IP address bytes 1 through 4
            #        group = match.group(g)
            #        intipa += (int(group),)
            #    group = match.group(5) # optional mask
            #    if group:
            #        intipa += (int(group),)
            #    else:
            #        intipa += (32,)
            #    ips.append(intipa)
            #else:
            #    print(
            #        "!!! The IP address pattern", pattern, 
            #        "does not match the line", line
            #    )
            if '/' not in line: # an IPA without a mask
                if ':' not in line: # an IPv4 address
                    line += '/32'
                else:               # an IPv6 address
                    line += '/128'
            try:
                try:
                    try:
                        ipa = ipaddress.ip_network(line, False)
                        if isinstance(ipa, ipaddress.IPv4Network):
                            ipas[IPV4].append(ipa)
                        elif isinstance(ipa, ipaddress.IPv6Network):
                            ipas[IPV6].append(ipa)
                    except ipaddress.AddressValueError as address_error:
                        print('Address value error wile parsing address', line)
                        print('File:', filename)
                        print('Message:', str(address_error))
                except ipaddress.NetmaskValueError as netmask_error:
                    print('Netmask value error wile parsing address', line)
                    print('File:', filename)
                    print('Message:', str(netmask_error))
            except ValueError as value_error:
                print('Value error wile parsing address', line)
                print('File:', filename)
                print('Message:', str(value_error))
    return ipas

#def format_ipa(ipa):
#    #return "{}.{}.{}.{}/{}".format(ipa[0], ipa[1], ipa[2], ipa[3], ipa[4])
#    return str(ipa)

def get_all_ipas():
    all_ipas = {
        IPV4: [],
        IPV6: []
    }
    filenames = get_filenames()
    for f in filenames:
        file_ipas = get_ipas_from_file(f)
        all_ipas[IPV4] += file_ipas[IPV4]
        all_ipas[IPV6] += file_ipas[IPV6]
    ipa_list = lambda x: list(reversed(sorted(set(x))))
    result = \
        ipa_list(all_ipas[IPV4]) + \
        ipa_list(all_ipas[IPV6])
    return result

def block_all_ipas():
    all_ipas = get_all_ipas()
    for ipa in all_ipas:
        #stripa = format_ipa(ipa)
        stripa = str(ipa)
        command = 'sudo ufw prepend deny from %s' % stripa
        print('Blocking access from the IP-address (range)', stripa, '…')
        print('Command:', command)
        os.system(command)

if __name__ == "__main__":
    block_all_ipas()
